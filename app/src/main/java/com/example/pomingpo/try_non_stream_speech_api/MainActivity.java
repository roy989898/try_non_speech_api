package com.example.pomingpo.try_non_stream_speech_api;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tvShowText;
    private Button btStart;
    private Button btFinsih;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();

        btFinsih.setOnClickListener(this);
        btStart.setOnClickListener(this);

    }

    private void initView() {
        tvShowText = (TextView) findViewById(R.id.tv_show_text);
        btStart = (Button) findViewById(R.id.bt_start);
        btFinsih = (Button) findViewById(R.id.bt_finsih);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_start:
                break;

            case R.id.bt_finsih:
                break;
        }

    }
}
